# poc-quantime-backend

# Using Locally
* `$ pip install -r requirements.txt`
* `$ python app.py`

# Live Version
* A live version of this backend is running at `https://folder-time-tracker.herokuapp.com/`

# Available Endpoints

* GET `/<string:foldername`: stores the timestamp with name `folder` to record the current folder at the time of request
* GET `/`: Returns the history of file usage
* GET `/getmeallapps`: Returns history of app usage
* GET `/app/<string :appname`:  stores the timestamp with name `appname` to record the current app at the time of request


# Note:
* I have reset the db. To fetch results, run the client script for a while(2-3mins) on your machine.