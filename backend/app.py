import os
from flask import Flask 
from flask_restful import Api
from resources.eventresource import EventResource, AggregateResource
from resources.apptimeresource import Log,AppAggregateResource
app = Flask(__name__)
api = Api(app)
app.secretkey = 'trenton alphanso miriam trotsky'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL','sqlite:///data.db')
api.add_resource(EventResource, '/<string:folder>')
api.add_resource(AggregateResource,'/')
api.add_resource(AppAggregateResource,'/getmeallapps')
api.add_resource(Log,'/app/<string:appname>')


if __name__ == '__main__':
	from db import db 
	db.init_app(app)
	app.run(port=5000,debug=True)