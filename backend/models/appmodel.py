from db import db

class AppEvent(db.Model):

	__tablename__ = 'apptimetable'
	id = db.Column(db.Integer, primary_key=True)
	appname = db.Column(db.Text, nullable=False)
	created_on = db.Column(db.DateTime, server_default=db.func.now())

	def __init__(self, appname):

		self.appname = appname

	def save_to_db(self):

		db.session.add(self)
		db.session.commit()

	def delete(self):

		db.session.delete(self)
		db.session.commit()

	def json(self):
		return {'appname':self.appname,'time':str(self.created_on)}

	@staticmethod
	def get_everything():
		return apptimetable.query.all()
	