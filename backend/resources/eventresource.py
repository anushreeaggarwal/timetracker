from flask_restful import Resource, reqparse
from models.eventmodel import TimeEvent

class EventResource(Resource):
	def get(self,folder):
		event = TimeEvent(folder=str(folder))
		event.save_to_db()
		return {'message':'success'}

class AggregateResource(Resource):
	def get(self):
		result = TimeEvent.query.all()
		return {'result':[x.json() for x in result]}

