from flask_restful import Resource, reqparse
from models.appmodel import AppEvent

class Log(Resource):
	def get(self,appname):
		applog = AppEvent(appname=str(appname))
		applog.save_to_db()
		return {'message':'success'}

class AppAggregateResource(Resource):
	def get(self):
		result = AppEvent.query.all()
		return {'result':[x.json() for x in result]}


